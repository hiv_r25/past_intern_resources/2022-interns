# Starting up Jupyter on DCC OnDemand
1. Go to [DCC OnDemand](https://dcc-ondemand-01.oit.duke.edu/)
2. Sign in with your NetID and password
3. Click on **Interactive Apps** at the top and select **Jupyter Singularity** 
    - If you don't see **Interactive Apps** and only see three lines in the top right, click on the three lines, then **Interactive Apps**, then **Jupyter Singularity**
4. A new page should open that says **Jupyter Singularity** at the top.
    - For *Account* select "chsi-hivr25-interns-2022"
    - For *Partition* select "chsi"
    - For *Walltime* select "4"
    - For *Jupyter Lab or Jupyter Notebook?* select "Jupyter Lab"
    - For *Singularity Container File*, click on *Select Path* then select `/opt/apps/community/od_chsi_jupyter/flowkit-container-ood_v011.sif`
    - Leave everything else as it is
    - Scroll to the bottom and click **Launch**
5. A new page should open with a box that says **Jupyter Singularity**, you may see the following messages:
    - "Please be patient as your job currently sits in queue. The wait time depends on the number of cores as well as time requested."
    - "Your session is currently starting... Please be patient as this process can take a few minutes."
6. You are waiting for a blue button to appear that says **Connect to Jupyter**. This could take a minute or so. When it appears, click on it.
7. After a minute or so, Jupyter should open in your webbrowser.
8. Please continue with the instructions in the next section to clean up.

# Shutting Down
1. In Jupyter click on **File** in the top left, then select **Shut Down**
2. A box should pop up that says **Shutdown confirmation**, click on **Shut Down**
3. This should close the tab that contained the Jupyter session and return you the **My Interactive Sessions** tab
3. In the **My Interactive Sessions** tab,  click on the red **Delete** button in the **Jupyter Singularity** box, then click **Confirm** in the box that pops up.
4. You can now click on the logout button in the top right, which is an arrow pointing to the right.
5. You are done!
